job "resticgui_job" {

  datacenters = ["dc1"]

  type = "service"

  constraint {
    attribute = "${attr.unique.hostname}"
    value     = "^nomad.*"
    operator  = "regexp"
  }

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "resticgui" {
    count = 1
    network {
      mode = "bridge"
    }
    service {
      port = "3030"
      name = "resticgui"
      connect {
        sidecar_service {
           proxy {

           }
        }
      }
      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=true",
        "traefik.http.routers.to-demo-resticgui-http.entrypoints=web",
        "traefik.http.routers.to-demo-resticgui-http.rule=Host(`demo.resticgui.com`)",
        "traefik.http.routers.to-demo-resticgui-https.middlewares=private-ip@file",
        "traefik.http.routers.to-resticgui-http.entrypoints=web",
        "traefik.http.routers.to-resticgui-http.rule=Host(`resticgui.mhx.lan`)",
        "traefik.http.routers.to-resticgui-http.middlewares=force-https@file",
        "traefik.http.routers.to-resticgui-https.entrypoints=websecure",
        "traefik.http.routers.to-resticgui-https.rule=Host(`resticgui.mhx.lan`)",
        "traefik.http.routers.to-resticgui-https.middlewares=sec-header@file",
        "traefik.http.routers.to-resticgui-https.middlewares=private-ip@file",
        "traefik.http.routers.to-resticgui-https.tls=true",
      ]
    }
    
    task "resticgui" {
      driver = "docker"
      config {
        image = "resticgui/resticgui:v0.1.3"
        force_pull = "true"
      }
      resources {
        cpu    = 200 # MHz
        memory = 256 # MB
      }
    }

  }
}
